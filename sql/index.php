<?php

include('riksdagen_api.php');

//

$host = 'localhost';
$dbname = 'lg0014';
$username = 'lg0014';
$password = 'nfRJmLPdq9';

$mysql = new mysqli($host, $username, $password, $dbname);

if ($mysql->connect_error) {
    die("kunde inte ansluta till databasen" . $mysql->connect_error);
}


if (isset($_GET['do']) && $_GET['do'] == 'update')
{   
    // 1. Radera allt innehåll i tabellen rik_members. Använd TRUNCATE
    $sql = "TRUNCATE TABLE rik_members";

    $mysql->query($sql);

    // 2. Hämta den senaste datan via get_riksdagen_members(). Den levereras i form av en array.

    $members = get_riksdagen_members();

    // 3. Stoppa in den senaste datan i tabellen rik_members. Använd INSERT

    foreach ($members['personlista']['person'] as $member) {
        extract($member);
        $sql = "INSERT INTO rik_members (born, gender, last_name, first_name, party, state, image, updated_at) VALUES ($fodd_ar, '$kon', '$efternamn', '$tilltalsnamn', '$parti', '$valkrets', '$bild_url_192', NOW())";
        $mysql->query($sql);
    }

}

// Hämta data om ledamöterna från tabellen rik_members. Använd SELECT
$sql = "SELECT * FROM rik_members";

if (!empty($_GET['party'])) {
    $party = $_GET['party'];
    $sql.= " WHERE party = '$party'";
}

$rows = $mysql->query($sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="get">
        <input type="text" name="party">
        <input type="submit" value="Skicka">
    </form>
    <?php while($row = $rows->fetch_assoc()) { ?>
        <p>
            <h2><?= $row['first_name'] . ' ' .$row['last_name'] ?></h2>
            <img src = <?= $row['image']?>>
            <h4>född år <?= $row['born']?> <br>
            kön: <?= $row['gender']?> <br>
            parti: <?= $row['party']?> <br>
            län: <?= $row['state']?>
        </h4>
            
        </p>
    <?php } ?>
</body>
</html>

